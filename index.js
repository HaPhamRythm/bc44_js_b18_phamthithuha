// create array

numList = [];
function addNum() {
var num = document.querySelector("#num");
numList.push(num.value*1);
document.querySelector("#numList").innerHTML = numList
num.value = "";
}

// Ex1: total of the numbers in numList
sum1 = 0;
function totalNum() {
    for (var i=0; i<numList.length; i++){
    sum1 += numList[i];
    } 
    document.querySelector("#result1").innerHTML = sum1;
}

//Ex2: Count the number of positive numbers
count = 0;
function countPNum() {
    
    for (var i=0; i<numList.length; i++) {
        if(numList[i]>0) {
            count ++;
            
        }
    }
    document.querySelector("#result2").innerHTML = count;
}

// Ex3: Find min in the numList

function findMin() {
    var min = numList[0];
    for (var i=1; i<numList.length; i++) {
        if(numList[i] < min ) {
            min = numList[i];
        }
    }
    document.querySelector("#result3").innerHTML = min;
}

// Ex4: Find min of positive number in the numList

function findMinPNum() {
    positiveList = [];
    for (var i=0; i<numList.length; i++) {
        if(numList[i]>0) {
            positiveList.push(numList[i]);
            
        }
    var min4 = positiveList[0];
    for (var j=1; j<positiveList.length; j++) {
        if(positiveList[j] < min4 ) {
            min4 = positiveList[j];
        }
    }
}
    
    document.querySelector("#result4").innerHTML = min4;
}


// Ex5: Find the last even number in the numList

function findFEven() {
    evenNumList = [];
    
    for (var i=0; i<numList.length; i++) {
        if(numList[i]%2 === 0) {
            evenNumList.push(numList[i]);
            lastEvenNum = evenNumList[evenNumList.length-1];
            
        }
}
document.querySelector("#result5").innerHTML = lastEvenNum;
}

// Ex6: Exchange the index position of 2 numbers

function exchangeIndexPosition() {
    var num61 = document.querySelector("#num61").value*1;
    var num62 = document.querySelector("#num62").value*1;
    arrNum = [];
    arrNum.push(num61);
    arrNum.push(num62);
    newArrNum = [];
    newArrNum[0] = num62;
    newArrNum[1] = num61;
    document.querySelector("#result6").innerHTML = newArrNum;
}
// Ex7: Sort from the lowest to the greatest
function sortLG() {
    numList = numList.sort();
    document.querySelector("#result7").innerHTML = numList;
}

// Ex8: Find the first Prime Number
primeNumList = [];
function find1stPrimeNum() {
    
    
    for (var i=0; i < numList.length; i++){
        isPrimeNum = true;
        
        for (var j=2; j <= Math.sqrt(numList[i]); j++) {
            if(numList[i]% j ===0 ) {
                isPrimeNum = false;
            } 
                }

                if(isPrimeNum) {
                    primeNumList.push(numList[i]);
                }
                
    }
    if(isPrimeNum) {
    document.querySelector("#result8").innerHTML = primeNumList[0];
    } else {document.querySelector("#result8").innerHTML = `-1`;}
    
    
}

// Ex9: Count integers
numList9 = [];
function addNum9() {
   var num9 = document.querySelector("#num9");
   numList9.push(num9.value*1);
   document.querySelector("#numList9").innerHTML = numList9;
   num9.value = "";
}
count9 =0;
function countIntegers() {
for (var i=0; i < numList9.length; i++) {
if(Number.isInteger(numList9[i])) {
    count9++;
}
}
document.querySelector("#result9").innerHTML = count9;
}

//Ex10: Compare the number of positive and negative numbers
countP = 0;
countN = 0;
function compare() {
    for (i=0; i<numList.length; i++) {
        if(numList[i] > 0) {
        countP++;
        } else {countN++;}
    }

    if (countP > countN) {
        document.querySelector("#result10").innerHTML = `Số dương > Số âm`
    } else if (countP < countN) {
document.querySelector("#result10").innerHTML = `Số dương < Số âm`
    }
        
     else {
document.querySelector("#result10").innerHTML = `Số dương = Số âm`

     }
}
